package birds
{
	import com.greensock.TweenMax;
	import com.greensock.easing.*;
	
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	
	import mx.controls.Alert;
	import mx.core.IFlexDisplayObject;
	import mx.core.IVisualElement;
	import mx.managers.PopUpManager;
	import mx.managers.PopUpManagerChildList;
	
	import birds.ui.layout._preloader;
	import birds.ui.popup.RecordsTable;
	import birds.ui.popup.chooseCurrency;
	import birds.ui.popup.popupSendCoctail;
	import birds.ui.popup.withdrawVotes;
	import birds.ui.messages.Msg;
	
	public class MessageManager extends Sprite
	{
		public var app:DisplayObject = null;
		
		public static const ANIM_FADE:String = "fade";
		public static const ANIM_RETURN:String = "returnObj";

        public static const RECORDS:String = "show_records";
		
		public static const NOT_ENOUGHT_GOLD:Object = {message_type:"text", message:"Не хватает звезд!"};
		public static const NOT_ENOUGHT_DOLLARS:Object = {message_type:"text", message:"Не хватает денег!"};
		public static const CHANGE_VOTES:Object = {message_type:"text", message:"Вы можете обменять голоса на золотые монеты. Для этого нажмите пополнить слева от Вашего баланса вверху экрана."};
		public static const ACCOUNT_BLOCKED:Object = {message_type:"text", message:"Ваш аккаунт заблокирован за дебош. Попробуйте зайти чуть позже :)"};
		public static const CHOOSE_FRIEND:Object = {message_type:"text", message:"Чтобы отправить коктейль, выберите пожалуста друга."};
		public static const COCTAIL_SEND:Object = {message_type:"text", message:"Подарок отправлен!"};
		public static const NOT_ENOUGHT_VOTES:Object = {message_type:"text", message:"У Вас недостаточно голосов на счету!"};
		public static const SERVER_FAIL:Object = {message_type:"text", message:"Временные неполадки, попробуйте пожалуйста позже."};
		
		
		private var popup:IFlexDisplayObject;
		private var _loader:_preloader;
		
		private static var __instance:MessageManager = new MessageManager();
		public function MessageManager()
		{
			super();
			if (__instance != null)
			{
				throw new Error("Singleton can only be accessed through ItemsManager.instance");
			}
		}
		
		public static function get instance():MessageManager
		{
			return __instance;
		}
		
		/**
		 * addMessage
		 * @param text - message type /send_coctail, choose_currency, send_votes, send_vip, show_records/
		 * @param type - some indeses
		 * 
		 */		
		public function addMessage(text:String = "", value:int = 0, type:String = ""):void {
			
			switch(text) {
				case "send_coctail":
					popupSendCoctail.itemInList = value;
					popupSendCoctail.itemType = type;
					
					popup = PopUpManager.createPopUp(app, popupSendCoctail, false);
					popup.x = 600;
					popup.y = 105;
					
					TweenMax.to(popup, 1, {x:5, ease:Expo.easeInOut})
					
				break;
				
				case "choose_currency":
					popup = PopUpManager.createPopUp(app, chooseCurrency, true);
					popup.alpha = 0;
					
					TweenMax.to(popup, 1, {alpha:1, ease:Back.easeOut})
					
					PopUpManager.centerPopUp(popup);
				break;
				
				case "send_votes":
					withdrawVotes.currency = value;
					popup = PopUpManager.createPopUp(app, withdrawVotes, true);
					popup.alpha = 0;
					
					TweenMax.to(popup, 1, {alpha:1, ease:Back.easeOut})
					
					PopUpManager.centerPopUp(popup);
				break;
				
				case "send_vip":
					popupSendCoctail.itemInList = value;
					popupSendCoctail.itemType = type;
					
					popup = PopUpManager.createPopUp(app, popupSendCoctail, false);
					popup.x = 600;
					popup.y = 105;
					
					TweenMax.to(popup, 1, {x:5, ease:Expo.easeInOut});

					break;
				
				case RECORDS:
					popup = PopUpManager.createPopUp(app, RecordsTable, false);
					popup.alpha = 0;
					
					TweenMax.to(popup, 1, {alpha:1, ease:Expo.easeInOut});
					PopUpManager.centerPopUp(popup);
					
					break;
			}
	
		}
		
		/**
		 * removeMessage
		 * @param obj - our popup
		 * @param animationType - null, ANIM_FADE, ANIM_RETURN
		 * 
		 */
		public function removeMessage(obj:* = null, animationType:String = ""):void {
			
			if(obj != null) {
				TweenMax.to(obj, 0.5, {alpha:0, overwrite:true, ease:Back.easeOut, onComplete:function():void { PopUpManager.removePopUp(obj); }});
			} 
			if(popup != null) {				
				switch(animationType)
				{
					case ANIM_FADE:
					case "":
						TweenMax.to(popup, 0.5, {alpha:0, overwrite:true, ease:Back.easeOut, onComplete:function():void { PopUpManager.removePopUp(popup); }});
					break;
					case ANIM_RETURN:
						TweenMax.to(popup, 1.2, {x:-600, overwrite:true, ease:Expo.easeInOut, onComplete:function():void { PopUpManager.removePopUp(popup); }});
					break;
				}
			}
				
		}
		
		/**
		 * addSystemMessage() - show alerts with bonuses, gifts of simple text messages
		 * 
		 * @param _message - text message
		 * @param _visit - this is message with vivit bonus or not
		 * @param _showBtns - show OK button or not
		 * @param newWidth - width of alert
		 * @param newHeight - height of alert
		 * 
		 */
		public function addSystemMessage(_message:Object, _visit:Boolean = false, _showBtns:Boolean = false, newWidth:int = 305, newHeight:int = 156):void {
			Msg.show(_message, app, false, _visit, _showBtns, newWidth, newHeight);
		}
		
		/**
		 * hideSystemMessage
		 * 
		 */
		public function hideSystemMessage():void {
			Msg.hide();
			
		}
		
		public function showPreloader(textToShow:String = "", _x:int = 292, _y:int = 262):void {
			_loader = new _preloader();
			_loader.info = textToShow;
			_loader.x = _x;
			_loader.y = _y;
			
			app["preload"].addElement(_loader as IVisualElement);
		}
		
		public function hidePreloader():void
        {
			TweenMax.to(_loader, 0.1, {alpha:0, ease:Expo.easeInOut, onComplete:function():void { app["preload"].removeAllElements() }});
		}
	}
}