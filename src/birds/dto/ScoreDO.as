/**
 * Created by limestore on 07.01.14.
 */
package birds.dto
{
public class ScoreDO extends Object implements IScore
{
    private var _id:String;
    private var _name:String;
    private var _points:int;

    public function ScoreDO()
    {
    }

    public function get id():String
    {
        return _id;
    }

    public function set id(value:String):void
    {
        _id = value;
    }

    public function get name():String
    {
        return _name;
    }

    public function set name(value:String):void
    {
        _name = value;
    }

    public function get points():int
    {
        return _points;
    }

    public function set points(value:int):void
    {
        _points = value;
    }
}
}
