/**
 * Created by limestore on 07.01.14.
 */
package birds.dto
{
public interface IScore
{
    function get id():String;
    function set id(value:String):void;

    function get name():String;
    function set name(value:String):void;

    function get points():int;
    function set points(value:int):void;
}
}
