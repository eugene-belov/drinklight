package birds
{
import birds.dto.IScore;
import birds.dto.ScoreDO;

import com.adobe.serialization.json.JSON;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.net.registerClassAlias;
	
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	import birds.constants.GameGlobals;
	import sys.vk.APIConnection;
	
	public dynamic class Server extends Sprite
	{
		
		public static var coctailsList			:Object;
		public static var coctailVip			:Object;
		public static var coctailNotes			:Object;
		
		/**
		 * may contains next values : name(fname + lname), uid, photo 
		 */
		public static var app_users_info		:Array = [];
		public static var app_user_friends		:Array = [];
		
		/**
		 * app_user_info.vkid
		 * app_user_info.name
		 * app_user_info.alcohol
		 * app_user_info.experience
		 * app_user_info.gold 
		 * app_user_info.baks
		 * */
		[Bindable]
		public static var app_user_info			:Object = new Object;
		public static var all_user_photos		:Array = [];
		public static var API					:APIConnection;
		
		private	var amf							:RemoteObject;
		
		public function Server()
		{
			amf = new RemoteObject();
			amf.destination = "Server";
			amf.endpoint = GameGlobals.instance.urlGateway;
			amf.source = "vk_test.Server";
		}
		
		/**
		 * AMF_InitPlayer - get information about player
		 * @param user_id
		 * @param auth_id
		 * @return 
		 * 
		 */
		public function AMF_InitPlayer(user_id:Number, auth_id:String)						:RemoteObject 
		{
			amf.init(user_id, auth_id);  
			
			return amf; 
		}
		/* ---------------------------------------------------------------------------------------------- */
		
		/**
		 *AMF_GetSendedGift 
		 * @param vk_id
		 * @return 
		 * 
		 */
		public function AMF_GetCoctailsNotes()												:RemoteObject 
		{	
			
			amf.CallServerMethod(GameGlobals.instance.vkUserId, GameGlobals.instance.vkAuthKey, "GetAllDrinkSubscribe");
			
			return amf; 
		}
		/* ---------------------------------------------------------------------------------------------- */
		
		/**
		 *AMF_GetSendedGift 
		 * @param vk_id
		 * @return 
		 * 
		 */
		public function AMF_GetSendedCoctail()												:RemoteObject 
		{	
			
			amf.CallServerMethod(GameGlobals.instance.vkUserId, GameGlobals.instance.vkAuthKey, "GetSendedGift");
			
			return amf; 
		}
		/* ---------------------------------------------------------------------------------------------- */
		
		/**
		 *AMF_GetSendedVIPCoctail 
		 * @param vk_id
		 * @return 
		 * 
		 */
		public function AMF_GetSendedVIPCoctail()												:RemoteObject 
		{	
			
			amf.CallServerMethod(GameGlobals.instance.vkUserId, GameGlobals.instance.vkAuthKey, "GetSendedVipGift");
			
			return amf; 
		}
		/* ---------------------------------------------------------------------------------------------- */
		
		/**
		 *AMF_GetReceivedGift 
		 * @param vk_id
		 * @return 
		 * 
		 */
		public function AMF_GetReceivedCoctail()											:RemoteObject 
		{	
			
			amf.CallServerMethod(GameGlobals.instance.vkUserId, GameGlobals.instance.vkAuthKey, "GetReceivedGift");
			
			return amf; 
		}
		/* ---------------------------------------------------------------------------------------------- */
		
		/**
		 *AMF_GetReceivedVIPCoctail 
		 * @param vk_id
		 * @return 
		 * 
		 */
		public function AMF_GetReceivedVIPCoctail()											:RemoteObject 
		{	
			
			amf.CallServerMethod(GameGlobals.instance.vkUserId, GameGlobals.instance.vkAuthKey, "GetReceivedVipGift");
			
			return amf; 
		}
		/* ---------------------------------------------------------------------------------------------- */
		
		/**
		 * AMF_GetDrinksList - get user info from the server
		 * @return 
		 * 
		 */
		public function AMF_GetUserInfo()													:RemoteObject 
		{	
			
			amf.CallServerMethod(GameGlobals.instance.vkUserId, GameGlobals.instance.vkAuthKey, "GetUserInfo");
			
			return amf; 
		}
		/* ---------------------------------------------------------------------------------------------- */
		
		/**
		 * AMF_GetDrinksList - get drinks list from the server
		 * @return 
		 * 
		 */
		public function AMF_GetCoctailsList()												:RemoteObject 
		{ 
			amf.CallServerMethod(GameGlobals.instance.vkUserId, GameGlobals.instance.vkAuthKey, "GetAllDrinks");
			
			return amf;
		}
		/* ---------------------------------------------------------------------------------------------- */
		
		/**
		 * AMF_GetVIPDrinksList - get VIP drinks list from the server
		 * @return 
		 * 
		 */
		public function AMF_GetVIPCoctailsList()											:RemoteObject 
		{ 
			amf.CallServerMethod(GameGlobals.instance.vkUserId, GameGlobals.instance.vkAuthKey, "GetAllVipDrinks");
			
			return amf;
		}
		/* ---------------------------------------------------------------------------------------------- */
		
		/**
		 * AMF_GetVipCoctail - get drinks list from the server
		 * @return 
		 * 
		 */
		public function AMF_GetVipCoctail()													:RemoteObject 
		{ 
			amf.CallServerMethod(GameGlobals.instance.vkUserId, GameGlobals.instance.vkAuthKey, "GetTodayVipDrink");
			
			return amf;
		}
		/* ---------------------------------------------------------------------------------------------- */
		
		/**
		 * AMF_GetAppUserBalance - get user app balance
		 * @return 
		 * 
		 */
		public function AMF_GetAppUserBalance()												:RemoteObject 
		{ 
			amf.CallServerMethod(GameGlobals.instance.vkUserId, GameGlobals.instance.vkAuthKey, "getAppUserBalance");
			
			return amf;
		}
		/* ---------------------------------------------------------------------------------------------- */
		
		/**
		 * AMF_GetAppUserBalance - get user app balance
		 * @return 
		 * 
		 */
		public function AMF_UpdateAnswerMessagesFromServer()								:RemoteObject 
		{ 
			amf.CallServerMethod(GameGlobals.instance.vkUserId, GameGlobals.instance.vkAuthKey, "Time");
			
			return amf;
		}
		/* ---------------------------------------------------------------------------------------------- */
		
		/**
		 * AMF_SendCoctailTo - send coctail
		 * @param params, for ex. {"uids":"7772031,66666","drink_id":"8","id_message":"4"}
		 * @return 
		 * 
		 */
		public function AMF_SendCoctailTo(params:Object)									:RemoteObject 
		{ 
			amf.CallServerMethod(GameGlobals.instance.vkUserId, GameGlobals.instance.vkAuthKey, "SendGift", params);
			
			return amf;
		}
		/* ---------------------------------------------------------------------------------------------- */
		
		/**
		 * AMF_SendVIPCoctailTo - send coctail
		 * @param params, for ex. {"uids":"7772031,66666","drink_id":"8","id_message":"4"}
		 * @return 
		 * 
		 */
		public function AMF_SendVIPCoctailTo(params:Object)									:RemoteObject 
		{ 
			amf.CallServerMethod(GameGlobals.instance.vkUserId, GameGlobals.instance.vkAuthKey, "SendVipGift", params);
			
			return amf;
		}
		/* ---------------------------------------------------------------------------------------------- */
		
		/**
		 * AMF_SendVotesToApp - send votes to our application
		 * * @param params, for ex. {"to":"gold|baks","count":"number 1-n"}
		 * @return 
		 * 
		 */
		public function AMF_SendVotesToApp(params:Object)									:RemoteObject 
		{ 
			amf.CallServerMethod(GameGlobals.instance.vkUserId, GameGlobals.instance.vkAuthKey, "votesToPoints", params);
			
			return amf;
		}
		/* ---------------------------------------------------------------------------------------------- */
		
		/**
		 * AMF_GetExperienceRating - Get list of users with experience rating
		 * * @param params, for ex. {"uids":"uid,uid"}
		 * @return 
		 * 
		 */
		public function AMF_GetExperienceRating(params:Object)								:RemoteObject 
		{ 
			amf.CallServerMethod(GameGlobals.instance.vkUserId, GameGlobals.instance.vkAuthKey, "GetRatingsByExp", params);
			
			return amf;
		}
		/* ---------------------------------------------------------------------------------------------- */
		
		/**
		 * AMF_GetAlcoholRating - Get list of users with alcohol rating
		 * * @param params, for ex. {"uids":"uid,uid"}
		 * @return 
		 * 
		 */
		public function AMF_GetAlcoholRating(params:Object)									:RemoteObject 
		{ 
			amf.CallServerMethod(GameGlobals.instance.vkUserId, GameGlobals.instance.vkAuthKey, "GetRatingsByAlko", params);
			
			return amf;
		}
		/* ---------------------------------------------------------------------------------------------- */
		
		/**
		 * AMF_UpdateUserScore - update user money and experience on server
		 * * @param params, for ex. {"xp":"0","baks":""}		
		 * @return 
		 * 
		 */
		public function AMF_UpdateUserScore(params:Object)									:RemoteObject 
		{ 
			amf.CallServerMethod(GameGlobals.instance.vkUserId, GameGlobals.instance.vkAuthKey, "SetXp", params);
			
			return amf;
		}
		/* ---------------------------------------------------------------------------------------------- */
		
		/**
		 * AMF_UpdtaeTimeInGame - update time in game wich user spend
		 * * @param params, for ex. {"time":"213"}
		 * @return 
		 * 
		 */
		public function AMF_UpdateTimeInGame(params:Object)									:RemoteObject 
		{ 
			amf.CallServerMethod(GameGlobals.instance.vkUserId, GameGlobals.instance.vkAuthKey, "StatisticRecalcLinesTime", params);
			
			return amf;
		}
		/* ---------------------------------------------------------------------------------------------- */
		
		/**
		 * AMF_UpdtaeTimeInGame - send command to server if game is open by user
		 * 
		 * @return 
		 * 
		 */
		public function AMF_OpenLinesGame()													:RemoteObject 
		{ 
			amf.CallServerMethod(GameGlobals.instance.vkUserId, GameGlobals.instance.vkAuthKey, "StatisticRecalcLinesVisits");
			
			return amf;
		}
		/* ---------------------------------------------------------------------------------------------- */
		
		/**
		 * idToVkontakteName - convert vk_id to first_name + last_name
		 * @param id
		 * @return first_name + last_name
		 */
		public static function idToVkontakteName(id:Number):String {
			//var result:String = app_user_info.name;
			var result:String = id.toString();
			
			for each(var user:* in Server.app_users_info) {
				if(id == user.uid) {
					result = user.name;
					break;
				} 
				
			}
			
			return result;
		}
	}
	
}