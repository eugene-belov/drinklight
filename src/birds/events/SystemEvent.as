package birds.events
{
	import flash.events.Event;
	
	public class SystemEvent extends Event
	{
		
		public static var LINES_CREATED			:String = "linesCreated";
		
		public static var INITIALIZE_LIST		:String = "initializeList";
		public static var SEND_COCTAIL			:String = "sendCoctail";
		public static var HIDE_SPLASH_SCREEN	:String = "hideSplashScreen";
        public static var HIDE_SPLASH_SCREEN_EMIDIATELY:String = "hideSplashScreenNow";
		public static var HELP_WINDOW		    :String = "helpWindowCalled";
		public static var SETTINGS_WINDOW_INIT	:String = "settingsWindowCalled";
		
		public static var GET_USER_NAME			:String = "getUserName";
		
		public static var REFUND_BAKS			:String = "refundBaks";
		public static var REFUND_GOLD			:String = "refundGold";
		public static var REFUND_BAKS_BONUS		:String = "refundBaksBonus";
		public static var REFUND_GOLD_BONUS		:String = "refundGoldBonus";
		
		public static var SHOW_ALCOHOL			:String = "alcohol";
		public static var SHOW_NON_ALCOHOL		:String = "nonAlcohol";
		
		public static var UPDATE_ALCOHOL		:String = "updateAlcohol";
		public static var UPDATE_CASH			:String = "updateCash";
		public static var UPDATE_STARS			:String = "updateStars";
		
		public static var VIP_IS_OVERDUE		:String = "giftIsOverdue";
		public static var VIP_IS_OKEY			:String = "giftIsOkey";
		
		public static var CLOSE_MESSAGE			:String = "closeMessage";

        public static var AD_DONE               :String = "advertismentFinish";
		
		public static var PAGE_COCTAILS			:int = 0;
		public static var PAGE_RECEIVED			:int = 1;
		public static var PAGE_SENDED			:int = 2;
		public static var PAGE_RATINGS			:int = 3;
		public static var PAGE_LINES			:int = 4;
		public static var PAGE_VIP				:int = 5;
		
		public var data							:Object = null;
		
		public function SystemEvent(type:String, _data:Object = null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			data = _data
		}
		
		// Override the inherited clone() method.
		override public function clone():Event {
			return new SystemEvent(type, data, bubbles, cancelable);
		}
	}
}