package birds.constants
{
import birds.Server;
import birds.dto.IScore;
import birds.dto.ScoreDO;

import flash.external.ExternalInterface;

public class GameGlobals
{

    private static var __instance:GameGlobals = new GameGlobals();

    public function GameGlobals()
    {
        super();
        if (__instance != null)
        {
            throw new Error("Singleton can only be accessed through ItemsManager.instance");
        }
    }

    public static function get instance():GameGlobals { return __instance; }

    private var _vkUserId:Number = 0;
    private var _vkAuthKey:String = "";
    private var _vkSecret:String;
    private var _ballCosts:Number = 0.02; // Kurs valuty
    private var _urlGateway:String = "http://aagq278a.facebook.joyent.us/lines/vk/gateway.php";//"http://workunion-games.ru/vk/gateway.php";
    private var _urlXmlVotes:String = "http://workunion-games.ru/vk/votes.xml";//"http://aagq278a.facebook.joyent.us/lines/flash_1/assets/votes.xml";

    private var _scores:Vector.<IScore> = new Vector.<IScore>();


    public function get vkSecret():String { return _vkSecret; }

    public function set vkSecret(value:String):void { _vkSecret = value; }

    public function get vkUserId():Number { return _vkUserId; }

    public function set vkUserId(value:Number):void { _vkUserId = value; }

    public function get vkAuthKey():String { return _vkAuthKey; }

    public function set vkAuthKey(value:String):void { _vkAuthKey = value; }

    public function get ballCosts():Number { return _ballCosts; }

    public function set ballCosts(value:Number):void { _ballCosts = value;}

    public function get urlGateway():String { return _urlGateway; }

    public function set urlGateway(value:String):void { _urlGateway = value; }

    public function get urlXmlVotes():String { return _urlXmlVotes; }

    public function set urlXmlVotes(value:String):void { _urlXmlVotes = value; }

    public function set scores(value:*):void
    {
        var array:Array = [];
        for (var item:* in value)
        {
            var scoreRecord:IScore = new ScoreDO();
            scoreRecord.id = value[item].id;
            scoreRecord.name = Server.idToVkontakteName(value[item].id);
            scoreRecord.points = value[item].points;
            array.push(scoreRecord);
        }

        array.sortOn("points", Array.NUMERIC | Array.DESCENDING);

        _scores = null;
        _scores = new Vector.<IScore>();

        while (array.length > 0) _scores.push(array.shift());
    }

    public function get scores():Vector.<IScore>
    {
        return _scores;
    }

    public function sortScoresDescending():void
    {
        var array:Array = [];

        while(_scores.length > 0) array.push(_scores.pop());
        array.sortOn("points", Array.NUMERIC | Array.DESCENDING);
        while(array.length > 0) _scores.push(array.shift());
    }

    public function jsonscores():Object
    {
        var result:Object = new Object();
        result.scope = new Object();

        for (var item:* in _scores)
        {
            result.scope["record" + item] = new Object();
            result.scope["record" + item].id = _scores[item].id;
            result.scope["record" + item].points = _scores[item].points;
        }

        return result;
    }

    public function log(value:Object):void
    {
        ExternalInterface.call("console.log", value);
    }

}
}