package birds.ui.layout
{
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Loader;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.system.System;
import flash.utils.ByteArray;

import mx.core.mx_internal;
import mx.events.FlexEvent;
import mx.graphics.codec.PNGEncoder;
import mx.rpc.events.ResultEvent;
import mx.rpc.remoting.RemoteObject;

import spark.components.BorderContainer;
import spark.components.Group;

import birds.MessageManager;
import birds.Server;
import birds.events.SystemEvent;
import sys.encode.Base64;

import birds.ui.coctails.renderers.Coctail;

public class SendCoctailClass extends BorderContainer
{
    [Bindable]
    public var itemsList:Group;

    private var cctl:Coctail;
    private var alcohol:Array = [];
    private var nonalcohol:Array = [];
    private var _X:int = 0;
    private var _Y:int = -198;
    private var newestCoctailDate:String = "";


    public function SendCoctailClass()
    {
        addEventListener(FlexEvent.CREATION_COMPLETE, initStage);
    }

    private function initStage(e:FlexEvent):void
    {

        //VIPInitiated();

        NotesInitiated();

        // Init coctail list first time
        createCoctailList(Server.coctailsList);

        MessageManager.instance.addEventListener(SystemEvent.INITIALIZE_LIST, function (e:SystemEvent):void
        {
            if (e.data == SystemEvent.PAGE_COCTAILS)
            {
                filterCoctailList(Server.coctailsList);
            }
        });

        MessageManager.instance.addEventListener(SystemEvent.SHOW_ALCOHOL, filterAlcoholCoctails);
        MessageManager.instance.addEventListener(SystemEvent.SHOW_NON_ALCOHOL, filterNonAlcoholCoctails);

    }

    /**
     * NotesInitiated
     * Load all NOTES to coctails and put the data to coctailNotes variable
     */
    private function NotesInitiated():void
    {
        var amf:Server = new Server();
        amf.AMF_GetCoctailsNotes().addEventListener(ResultEvent.RESULT, loaded);

        function loaded(e:ResultEvent):void
        {
            var result:Object = e.result.message;
            // Save all Notes to coctails to global Object
            Server.coctailNotes = result;

            e.currentTarget.removeEventListener(ResultEvent.RESULT, loaded);
        }
    }

    /**
     * createCoctailList
     * @param items
     *
     */
    private function createCoctailList(items:Object):void
    {
        MessageManager.instance.hidePreloader();

        if (itemsList.numElements > 0)
            itemsList.removeAllElements();

        _X = 0;
        _Y = -198;

        newestCoctailDate = items[1].сreated_date;

        for (var i:int = 0; i < items.length; i++)
        {
            if ((Number(items[i].xp) < 0))
                nonalcohol.push(items[i]);
            else
                alcohol.push(items[i]);

            addCoctailToStage(items, i);
        }
    }

    /**
     * allCoctailList
     * @param items
     *
     */
    protected function filterCoctailList(items:Object):void
    {

        if (itemsList.numElements > 0)
            itemsList.removeAllElements();

        _X = 0;
        _Y = -198;

        for (var i:int = 0; i < items.length; i++)
        {
            addCoctailToStage(items, i);
        }
    }


    /**
     * Filter all alcohol coctails
     * @param event
     *
     */
    protected function filterAlcoholCoctails(event:SystemEvent = null):void
    {

        if (itemsList.numElements > 0)
            itemsList.removeAllElements();

        _X = 0;
        _Y = -198;
        for (var i:int = 0; i < alcohol.length; i++)
        {
            addCoctailToStage(alcohol, i);
        }

    }

    /**
     * Filter all non alcohol coctails
     * @param event
     *
     */
    protected function filterNonAlcoholCoctails(event:SystemEvent = null):void
    {

        if (itemsList.numElements > 0)
            itemsList.removeAllElements();

        _X = 0;
        _Y = -198;
        for (var i:int = 0; i < nonalcohol.length; i++)
        {
            addCoctailToStage(nonalcohol, i);
        }
    }


    protected function addCoctailToStage(items:Object, i:int):void
    {
        var current_date:String = items[i].сreated_date;
        cctl = new Coctail();
        itemsList.addElement(cctl);
        cctl.coctailInfo = items[i].title;

        // Load Base64 coded image and encode it
        var bmp:ByteArray = Base64.decodeToByteArray(items[i].img);
        cctl.item = getCoctailPositionInArray(items[i].id);

        if (newestCoctailDate != null && newestCoctailDate != "0000-00-00")
        {
            if (newestCoctailDate == current_date && items[i].is_hit == 0)
            {
                cctl.newCoctail = true;
            }
            else
            {
                cctl.newCoctail = false;
            }
        }

        cctl.coctailOfTheDay = (items[i].is_hit == 1) ? true : false;
        cctl.coctailImage.source = bmp;
        if (items[i].is_gold == "1")
        {
            cctl.coctailPrice = items[i].price;
            cctl.priceInGold = true;
        }
        else
        {
            cctl.coctailPrice = items[i].price;
            cctl.priceInGold = false;
        }
        cctl.mouseChildren = false;
        cctl.mouseEnabled = true;
        cctl.buttonMode = true;

        cctl.addEventListener(MouseEvent.CLICK, chooseCoctailToSend);

        cctl.addEventListener(MouseEvent.MOUSE_OVER, function (e:MouseEvent):void
        {
            e.currentTarget.info.styleName = "coctailLabel_hover";
        });
        cctl.addEventListener(MouseEvent.MOUSE_OUT, function (e:MouseEvent):void
        {
            e.currentTarget.info.styleName = "coctailLabel";
        });

        if (i % 3 == 0)
        {
            _X = 10;
            _Y += 196;
        }
        else
        {
            _X += 190;
        }

        cctl.x = _X;
        cctl.y = _Y;
    }


    /**
     * chooseCoctailToSend
     * @param e
     *
     */
    private function chooseCoctailToSend(e:MouseEvent):void
    {

        if (Server.coctailsList[e.currentTarget.item].is_gold == "1" && Number(Server.coctailsList[e.currentTarget.item].price) > Number((MessageManager.instance.app as VK_DrinkFriend).cashPanel.stars.text))
            MessageManager.instance.addSystemMessage(["Не хватает звезд!"]);
        else if (Server.coctailsList[e.currentTarget.item].is_gold == "0" && Number(Server.coctailsList[e.currentTarget.item].price) > Number((MessageManager.instance.app as VK_DrinkFriend).cashPanel.baks.text))
            MessageManager.instance.addSystemMessage(["Не хватает баксов!"]);
        else
            MessageManager.instance.addMessage("send_coctail", e.currentTarget.item, "notvip");
    }


    /**
     * getCoctailPositionInArray() - return selected coctail index from All coctails array
     *
     * @param shoItemId : int
     * @return
     *
     */
    protected function getCoctailPositionInArray(shoItemId:int):int
    {

        var position:int = -1;

        for (var index:int = 0; index < Server.coctailsList.length; index++)
        {
            if (Server.coctailsList[index].id == shoItemId)
                position = index;
        }

        return position;
    }
}
}