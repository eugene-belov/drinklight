package birds.ui.datamodel
{
import flash.geom.Point;

[Bindable]
public class WithdrawVotesData
{
    public var btn1_star_money:String = "";
    public var btn1_star_votes:String = "";
    public var btn1_star_offsets:Point;

    public var btn2_star_money:String = "";
    public var btn2_star_votes:String = "";
    public var btn2_star_bonus:String = "";
    public var btn2_star_offsets:Point;

    public var btn3_star_money:String = "";
    public var btn3_star_votes:String = "";
    public var btn3_star_bonus:String = "";
    public var btn3_star_offsets:Point;


    public var btn1_baks_money:String = "";
    public var btn1_baks_votes:String = "";

    public var btn2_baks_money:String = "";
    public var btn2_baks_votes:String = "";
    public var btn2_baks_bonus:String = "";

    public var btn3_baks_money:String = "";
    public var btn3_baks_votes:String = "";
    public var btn3_baks_bonus:String = "";


    public function WithdrawVotesData()
    {
    }
}
}