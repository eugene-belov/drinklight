package birds.ui.lines
{
import com.greensock.TweenMax;

import flash.display.MovieClip;
import flash.media.Sound;
import flash.media.SoundChannel;
import flash.media.SoundTransform;
import flash.utils.setTimeout;

import mx.core.IVisualElement;
import mx.core.UIComponent;

public class JumpSoundManager
{
    [Bindable]
    private static var mcWithSnd:MovieClip;
    private static var sound_delay:int = 250;
    private static var jump:JumpSound = new JumpSound();
    private static var channel1:SoundChannel;
    private static var channel2:SoundChannel;
    private static var soundClip:SoundClip = new SoundClip();

    public static var isMuted:Boolean = true;

    public function JumpSoundManager() { trace("JumpSoundManager -> constructor") }


    public static function playJumpSound(target:MovieClip):void
    {
        if (channel1 == null && mcWithSnd == null && !isMuted)
        {
            channel1 = jump.play(0, 999);
        }
        else if (channel1 != null && !isMuted)
        {
            //channel1.stop();
            TweenMax.to(channel1, 0.2, {volume: 0, onComplete: function ():void
            {
                channel1.stop();
                channel1 = null;
            }})

            setTimeout(function ():void
            {
                channel2 = jump.play(0, 999);
            }, sound_delay);

        }
        else if (channel2 != null && !isMuted)
        {
            //channel2.stop();
            TweenMax.to(channel2, 0.2, {volume: 0, onComplete: function ():void
            {
                channel2.stop();
                channel2 = null;
            }});

            setTimeout(function ():void
            {
                channel1 = jump.play(0, 999);
            }, sound_delay);
        }

        mcWithSnd = target;

    }

    public static function killAllSounds():void
    {
        isMuted = true;

        if (channel1 != null) channel1.stop();
        channel1 = null;
        if (channel2 != null) channel2.stop();
        channel2 = null
    }

    public static function stopJumpingSound(_delay:Number = 0):void
    {
        if (channel1 != null) TweenMax.to(channel1, 0.3, {volume: 0, delay: _delay - 0.1, onComplete: function ():void { channel1.stop(); }})
        if (channel2 != null) TweenMax.to(channel2, 0.3, {volume: 0, delay: _delay - 0.1, onComplete: function ():void { channel2.stop(); }})

        mcWithSnd = null;
    }

    public static function runAllSounds():void
    {
        isMuted = false;

        if (channel1 != null) channel1 = jump.play(channel1.position, 999);
        else if (channel2 != null) channel2 = jump.play(channel2.position, 999);
        else if (mcWithSnd != null) channel1 = jump.play(0, 999);
    }
}
}