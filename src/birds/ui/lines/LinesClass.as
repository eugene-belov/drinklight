package birds.ui.lines
{
import birds.MessageManager;
import birds.Server;
import birds.constants.GameGlobals;
import birds.dto.IScore;
import birds.dto.ScoreDO;
import birds.events.SystemEvent;

import com.greensock.OverwriteManager;
import com.greensock.TweenMax;
import com.greensock.easing.*;
import com.greensock.plugins.*;

import flash.display.MovieClip;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.net.URLLoader;
import flash.net.URLRequest;

import mx.collections.ArrayCollection;
import mx.controls.Image;
import mx.core.IVisualElement;
import mx.events.FlexEvent;

import spark.components.BorderContainer;
import spark.components.Group;
import spark.components.RichText;
import spark.filters.GlowFilter;

public class LinesClass extends BorderContainer
{
    [Embed(source="/assets/game/blue_n.png")]
    [Bindable]
    public var darkblue:Class;

    [Embed(source="/assets/game/lightblue_n.png")]
    [Bindable]
    public var lightblue:Class;

    [Embed(source="/assets/game/darkred_n.png")]
    [Bindable]
    public var darkred:Class;

    [Embed(source="/assets/game/red_n.png")]
    [Bindable]
    public var red:Class;

    [Embed(source="/assets/game/green_n.png")]
    [Bindable]
    public var green:Class;

    [Embed(source="/assets/game/pink_n.png")]
    [Bindable]
    public var pink:Class;

    [Embed(source="/assets/game/yellow_n.png")]
    [Bindable]
    public var yellow:Class;


    [Bindable]
    public var score:Number = 0;
    [Bindable]
    public var money:Number = 0;

    public var nextDropColors:NextColors;
    public var gameOverBlocker:BorderContainer;
    public var tf_pretenderName:RichText;
    public var scoreUserField:RichText;
    public var scoreAdds:RichText;
    public var moneyTable:RichText;
    public var fieldUI:Group;
    public var pretenderBird:PretenderBirdAnimation;
    public var kingBird:KingBirdAnimation;

    private var birdAnimation:AnimationTimer;

    private var cellUI:Cell;
    private var time:Date;

    private var cellSize:int = 40;
    private var field:Array;

    private var _spacer:Number = 1;
    private var _score:Number = 0;

    private var _lines:Number = 0;
    private var _currentX:Number = -999;
    private var _currentY:Number = -999;
    private var _enabled:Boolean = true;
    private var _pathNotEmpty:Boolean = true;
    private var _freeCells:Number = 0;

    private var coordsOfKillingBalls:Array = [];

    private var birdsPresentColors:Array = [];
    private var birdsNextColors:Array = [];
    private var birdColors:Array = ["empty", red, darkblue, lightblue, darkred, green, yellow, pink];
    private var birdMcNames:Array = ["empty", "red", "darkblue", "lightblue", "darkred", "green", "yellow", "pink"];

    private var start:Object;
    private var end:Object;

    private var LINE_LENGTH:Number = 5;
    private var NEWBALLS_COUNT:Number = 3;
    private var FIELD_WIDTH:Number = 9;
    private var FIELD_HEIGHT:Number = 9;
    private var COLOR_QTY:Number = birdColors.length - 1;

    /* Constants of labels in a amination lib */
    private const BIRD_HMUR:String = "hmur";
    private const BIRD_JUMP:String = "jump";
    private const BIRD_MORGAET:String = "morgaet";
    private const BIRD_NEGATIVE:String = "negative";
    private const BIRD_SLEEP:String = "sleep";
    private const PRETENDER_BEER:String = "beer";
    private const PRETENDER_STEP:String = "step";
    private const PRETENDER_VZGLYAD:String = "vzglyad";
    private const KING_MORGAET:String = "morgaet";
    private const KING_POSOH:String = "posoh";
    private const KING_VZDIHAET:String = "vzdihaet";
    private const KING_KORONA:String = "korona";

    private var selectCellGlow:GlowFilter = new GlowFilter(0x517feb, 1, 7, 7, 1, 6);
    private var unselectCellGlow:GlowFilter = new GlowFilter(0xf12a0f, 0, 3, 3, 1, 6);

    [Bindable]
    public var kingName:String = "";
    [Bindable]
    public var pretenderName:String = "";
    [Bindable]
    public var highScore:String = "0000000";
    public var rateList:Vector.<IScore> = new Vector.<IScore>();

    /**
     * This class represent the logic of Classic Lines game
     *
     * <br /><br />
     * Various values:
     * @param field.allow - mark the cell, if it has ball or we can walk to it <br />
     *        -9 - ball presents; 0 - can wall; -1 - end of path; -999 - current point of selected ball;
     *
     * @param field.ballColor - here we save the index of ball color
     *
     * @param field.Ball - stores the ball UIObject
     *
     * @param field.cellUI - stores the cell UIObject
     *
     */

    public function LinesClass()
    {
        addEventListener(FlexEvent.CREATION_COMPLETE, beginGame);

        OverwriteManager.init();
        TweenPlugin.activate([FrameLabelPlugin, FramePlugin]);
    }

    //    TODO:remove when release
    protected function iAmWinner_temp(event:MouseEvent = null):void
    {
        score = 400;
        bitTheRecord(score)
    }

    //    TODO:remove when release
    protected function initStorage_temp(event:MouseEvent = null):void
    {
        var scope:Object = {
            scope: {
                record1: {id: "3917572", points: "1000"},
                record2: {id: "0", points: " * "},
                record3: {id: "0", points: " * "},
                record4: {id: "0", points: " * "},
                record5: {id: "0", points: " * "},
                record6: {id: "0", points: " * "},
                record7: {id: "0", points: " * "},
                record8: {id: "0", points: " * "},
                record9: {id: "0", points: " * "},
                record10: {id: "0", points: " * "},
                record11: {id: "0", points: " * "},
                record12: {id: "0", points: " * "},
                record13: {id: "0", points: " * "},
                record14: {id: "0", points: " * "},
                record15: {id: "0", points: " * "},
                record16: {id: "0", points: " * "}
            }
        };

        var api_query:String = "https://api.vk.com/method/storage.set?uid=" + GameGlobals.instance.vkUserId +
                "&key=scope&value=" + JSON.stringify(scope) +
                "&global=1&access_token=" + GameGlobals.instance.vkSecret;

        var api_request:URLRequest = new URLRequest(api_query);
        var api_loader:URLLoader = new URLLoader();
        api_loader.load(api_request);
    }

    //    TODO:remove when release
    protected function getStorageInfo_temp():void
    {
        var api_query:String = "https://api.vk.com/method/storage.get?uid=" + GameGlobals.instance.vkUserId +
                "&key=scope&global=1&access_token=" + GameGlobals.instance.vkSecret;
        var api_request:URLRequest = new URLRequest(api_query);
        var api_loader:URLLoader = new URLLoader();
        api_loader.addEventListener(Event.COMPLETE, function (event:Event):void
        {
            var result:Object = JSON.parse(event.currentTarget.data);
            if (result.response != "")
            {
                var records_l:Object = JSON.parse(result.response);
                trace(records_l);
            }
        });
        api_loader.load(api_request);
        api_loader.load(api_request);
    }


    /**
     * beginGame()
     * @param e
     *
     */
    public function beginGame(e:FlexEvent = null):void
    {
        if (fieldUI.numElements > 0)
            fieldUI.removeAllElements();

        //        initStorage_temp();
        //        getStorageInfo_temp();

        time = new Date();
        birdsNextColors = [];
        birdsPresentColors = [];
        nextDropColors.resetColors = true;

        start = new Object();
        end = new Object();
        birdAnimation = new AnimationTimer();
        money = 0;
        score = 0;

        _score = 0;
        _lines = 0;
        _currentX = -999;
        _currentY = -999;
        _freeCells = FIELD_WIDTH * FIELD_HEIGHT;

        gameOverBlocker.visible = false;

        // Формируем массив ячеек
        field = new Array();
        for (var z:int = 0; z < FIELD_HEIGHT; z++)
        {
            field[z] = new Array()
            for (var k:int = 0; k < FIELD_HEIGHT; k++)
            {
                field[z][k] = new Object();
            }
        }

        // Выстраиваем сетку
        for (var i:int = 0; i < FIELD_WIDTH; i++)
        {
            for (var j:int = 0; j < FIELD_HEIGHT; j++)
            {
                cellUI = new Cell();
                cellUI.name = i + "_" + j + " -> Cell";
                fieldUI.addElement(cellUI);
                cellUI.x = i * cellSize;
                cellUI.y = j * cellSize;

                cellUI.addEventListener(MouseEvent.MOUSE_DOWN, movePlayerBall);

                field[i][j].cellUI = cellUI;
                field[i][j].allow = 0;			// cell is walkable
                field[i][j].ballColor = 0;		// number of color. 0 = none

                cellUI.stateIndicator = String(field[i][j].allow);
            }
        }

        initScoresScope();

        createBalls();

        var animationKing:AnimationTimer = new AnimationTimer(kingBird);
        animationKing.startKingAnimation();

        var animationPretender:AnimationTimer = new AnimationTimer(pretenderBird);
        animationPretender.startPretenderAnimation();

        birdAnimation.startGameBirdsAnimation();
    }

    // Init balls
    /**
     * createBalls()
     * @param isFirstTime
     * @param isPlaying
     *
     */
    private function createBalls(isFirstTime:Boolean = true):void
    {
        var ballX:int = 0;
        var ballY:int = 0;
        var coords:Array = [];
        var temp:Array = [];

        while (_freeCells == 0 || coords.length < NEWBALLS_COUNT)
        {
            // Генерируем случайные координаты нашего гарика
            ballX = generateCoords().x;
            ballY = generateCoords().y;

            if (field[ballX][ballY].allow != -9 && field[ballX][ballY].ballColor == 0 && !temp[ballX + '_' + ballY])
            {
                coords.push({x: ballX, y: ballY});
            }

            temp[ballX + '_' + ballY] = true;

            if (_freeCells < NEWBALLS_COUNT && coords.length == _freeCells)
                break;
        }

        if (isFirstTime)
        {
            for (var i:int = 0; i < NEWBALLS_COUNT; i++)
            {
                // Если генерация происходит первый раз, то
                // генерируем цвета для шариков на сцене
                var color:* = Math.floor((Math.random() * COLOR_QTY) + 1);
                birdsPresentColors.push(color);

                // генерируем цвета для шариков в левой части
                var nextCol:* = Math.floor((Math.random() * COLOR_QTY) + 1);
                birdsNextColors.push(nextCol);

                (nextDropColors.getElementAt(i) as Image).source = birdColors[nextCol] as Class;

                placeBallToStage(coords[i].x, coords[i].y, color);
            }
        }
        else
        {
            //presentColors = nextColors;
            birdsPresentColors = birdsNextColors;

            // обнуляем масив будущих шариков
            birdsNextColors = [];

            for (var j:int = 0; j < NEWBALLS_COUNT && _freeCells > 0; j++)
            {
                // генерируем цвета для шариков в левой части
                var next:* = Math.floor((Math.random() * COLOR_QTY) + 1);
                birdsNextColors.push(next);

                (nextDropColors.getElementAt(j) as Image).source = birdColors[next] as Class;

                // Присваиваем массиву текущих шариковв на сцене, массив сгенерированных ранее
                placeBallToStage(coords[j].x, coords[j].y, birdsPresentColors[j]);

                if (_freeCells == 0)
                {
                    (nextDropColors.getElementAt(0) as Image).source = nextDropColors.empty;
                    (nextDropColors.getElementAt(1) as Image).source = nextDropColors.empty;
                    (nextDropColors.getElementAt(2) as Image).source = nextDropColors.empty;
                }
                if (_freeCells == 1)
                {
                    (nextDropColors.getElementAt(1) as Image).source = nextDropColors.empty;
                    (nextDropColors.getElementAt(2) as Image).source = nextDropColors.empty;
                }
                if (_freeCells == 2)
                    (nextDropColors.getElementAt(2) as Image).source = nextDropColors.empty;
            }
        }

        /**
         * If no free cell left, start new game and get data from server
         * about scores
         * */
        if (_freeCells == 0)
        {
            var anonimus:Function;

            gameOverBlocker.visible = true;

            MessageManager.instance.addSystemMessage(["За прошлую игру Вы заработали " + score + " очков!"]);
            MessageManager.instance.addEventListener(SystemEvent.CLOSE_MESSAGE, anonimus = function ():void
            {
                MessageManager.instance.removeEventListener(SystemEvent.CLOSE_MESSAGE, anonimus);

                var earnedMoney:String = (Number(Server.app_user_info.baks) + money).toString();
                Server.app_user_info.baks = Number(earnedMoney);

                updateUserScore(score);
                checkKing("restart-game");

                beginGame();
                score = 0;
            });

            return;
        }
    }

    private function placeBallToStage(ballX:int, ballY:int, color:int):void
    {
        _freeCells--;

        var tempCellUI:Cell = field[ballX][ballY].cellUI;
        var tempBallUI:Ball = new Ball();

        tempBallUI.name = ballX + "_" + ballY;

        field[ballX][ballY] = new Object();
        field[ballX][ballY].cellUI = tempCellUI;
        field[ballX][ballY].Ball = fieldUI.addElement(tempBallUI);
        field[ballX][ballY].Ball.x = ballX * cellSize + _spacer;
        field[ballX][ballY].Ball.y = ballY * cellSize + _spacer;
        field[ballX][ballY].allow = -9;
        field[ballX][ballY].ballColor = color;

        field[ballX][ballY].cellUI.stateIndicator = String(field[ballX][ballY].allow);

        // Add listener
        tempBallUI.addEventListener(MouseEvent.MOUSE_DOWN, selectBallItem);

        // Colorize ball
        var bird:MovieClip = (field[ballX][ballY].Ball as Ball).bird[birdMcNames[color]];
        bird.alpha = 1;

        bird.scaleX = bird.scaleY = 0.1;
        birdAnimation.addClip(bird);

        TweenMax.to(bird, 0.5, {scaleX: 1, scaleY: 1, ease: Back.easeOut});

        var getLines:Array = searchLines(ballX, ballY);

        if (getLines.length > 0)
        {
            coordsOfKillingBalls = [];

            for each(var coords:Object in getLines)
            {
                for each(var a:* in coords)
                {
                    coordsOfKillingBalls.push({x: a._x, y: a._y});
                    coordsOfKillingBalls.sortOn(["x", "y"]);
                }
            }

            removeBallsInLine(coordsOfKillingBalls);

            Unselect();
        }
    }


    /**
     * selectBallItem()
     * @param e
     * */
    private function selectBallItem(e:MouseEvent):void
    {

        if (!_enabled)
            return;

        var curX:int = Number(e.currentTarget.name.substr(0, 1));
        var curY:int = Number(e.currentTarget.name.substr(2, 1));

        if (field[curX][curY].allow != -9)
            return;

        // Set current coords to vars
        _currentX = curX;
        _currentY = curY;

        for (var i:int = 0; i < FIELD_WIDTH; i++)
        {
            for (var j:int = 0; j < FIELD_HEIGHT; j++)
            {
                if (field[i][j].allow != -9)
                {
                    // Mark cell wich can be used for Players action
                }
                else
                {
                    (field[i][j].Ball as Ball).filters = [unselectCellGlow];

                    // Animation
                    var bird_new:MovieClip = (field[i][j].Ball as Ball).bird[birdMcNames[field[i][j].ballColor]];
                    birdAnimation.unselectBird(bird_new);
                    birdAnimation.stopBirdRunning(bird_new);
                }
            }
        }

        // Animation
        var bird:MovieClip = (field[curX][curY].Ball as Ball).bird[birdMcNames[field[curX][curY].ballColor]];
        bird.gotoAndPlay(BIRD_JUMP);

        JumpSoundManager.playJumpSound(bird);

        birdAnimation.wakeUpAndSleep(bird);
        birdAnimation.beginBirdRunning(bird);

        (field[curX][curY].Ball as Ball).filters = [selectCellGlow];
    }


    /**
     * movePlayerBall()
     * @param e
     *
     */
    private function movePlayerBall(e:MouseEvent):void
    {
        var curX:int = Number(e.currentTarget.name.substr(0, 1));
        var curY:int = Number(e.currentTarget.name.substr(2, 1));


        // если флажок ячейки позволяет ходить то перемещаем шарик в новое место
        if ((_currentX != -999 && _currentY != -999) && field[curX][curY].allow != -9 && _enabled == true)
        {

            _enabled = false;

            field[curX][curY].allow = -1;
            field[curX][curY].cellUI.stateIndicator = String(field[curX][curY].allow);

            start = {x: _currentX, y: _currentY};
            end = {x: curX, y: curY};

            var objectCoords:ArrayCollection = new ArrayCollection();
            objectCoords.addItem(start);

            // Здесь мы получаем координаты конечной точки пути
            getFinishCoord(1, objectCoords);
            // Передаем координаты конечной точки и выстраиваем путь
            var path:ArrayCollection = makeOurPath(end);

            // Если путь найден
            if (path.getItemAt(0).label != "NO_PATH" && !_pathNotEmpty)
            {
                path.source.reverse();

                var coordToMove:Array = [];
                for each(var i:* in path)
                {
                    coordToMove.push({x: i.x * cellSize + _spacer, y: i.y * cellSize + _spacer})
                }

                // add begin path coords
                coordToMove.push({x: curX * cellSize + _spacer, y: curY * cellSize + _spacer});

                //Проверяем на длинну массива с найденным путем и чтобы в текущей ячейке был экземпляр класса Ball
                if (coordToMove.length > 0 && field[_currentX][_currentY].Ball != null)
                {
                    //Animation
                    var bird:MovieClip = (field[_currentX][_currentY].Ball as Ball).bird[birdMcNames[field[_currentX][_currentY].ballColor]];
                    bird.gotoAndPlay(BIRD_JUMP);

                    JumpSoundManager.stopJumpingSound(coordToMove.length * 0.15);

                    TweenMax.to(field[_currentX][_currentY].Ball, coordToMove.length * 0.15,
                            {    bezier: coordToMove,
                                ease: Sine.easeInOut,
                                onComplete: function ():void
                                {
                                    field[curX][curY].allow = -9;

                                    field[curX][curY].ballColor = field[_currentX][_currentY].ballColor;

                                    //field[_currentX][_currentY].cellUI.stateIndicator = String(field[_currentX][_currentY].allow);
                                    field[curX][curY].cellUI.stateIndicator = String(field[curX][curY].allow);

                                    // Берем старый экземпляр класса
                                    var tempBallContainer:Ball = field[_currentX][_currentY].Ball;
                                    // Присваиваем ему новое имя
                                    tempBallContainer.name = curX + "_" + curY;
                                    // Переназначаем экземпляр на новое место
                                    field[curX][curY].Ball = tempBallContainer;

                                    tempBallContainer.filters = [unselectCellGlow];

                                    var tempCell:Cell = field[_currentX][_currentY].cellUI;

                                    field[_currentX][_currentY] = new Object();
                                    field[_currentX][_currentY].cellUI = tempCell;
                                    field[_currentX][_currentY].allow = 0;
                                    field[_currentX][_currentY].ballColor = 0;

                                    // Шарик прибыл
                                    // Нам нужно опять заполнить свободные ячейки флагом allow = 0
                                    OnBallArrive(curX, curY);
                                }
                            });
                }
            }
            else
            {

                JumpSoundManager.stopJumpingSound();

                //Animation
                trace("Animation negative")
                var bird_no_path:MovieClip = (field[_currentX][_currentY].Ball as Ball).bird[birdMcNames[field[_currentX][_currentY].ballColor]];
                bird_no_path.gotoAndPlay(BIRD_NEGATIVE);

                // Если путь не найден, сбрасываем все
                Unselect();
            }
        }
    }

    /**
     * OnBallArrive()
     * @param x
     * @param y
     *
     */
    private function OnBallArrive(x:int, y:int):void
    {
        trace("Ball arrived: " + field[x][y].allow, x, y);
        _pathNotEmpty = true;

        // Animation
        var bird:MovieClip = (field[x][y].Ball as Ball).bird[birdMcNames[field[x][y].ballColor]];
        bird.gotoAndPlay(BIRD_MORGAET);

        var getLines:Array = searchLines(x, y);

        if (getLines.length > 0)
        {
            coordsOfKillingBalls = [];

            // Animation
            pretenderBird.gotoAndPlay(PRETENDER_BEER);
            kingBird.gotoAndPlay(KING_KORONA);

            for each(var coords:Object in getLines)
            {
                for each(var a:* in coords)
                {
                    coordsOfKillingBalls.push({x: a._x, y: a._y});
                    coordsOfKillingBalls.sortOn(["x", "y"]);
                }
            }

            removeBallsInLine(coordsOfKillingBalls);

            Unselect();
        }
        else
        {
            createBalls(false);
            Unselect();
        }

    }


    private function removeBallsInLine(ballsCoords:Array):void
    {
        var counter:int = 0;
        var delayAnimation:int = 0;
        var linePoints:int = 0;

        scoreAdds.text = "";

        // Считаем полученные очки за удаленные шарикиs
        linePoints = 50 + (ballsCoords.length - LINE_LENGTH) * 4;

        ( pretenderBird.visible ) ? score += linePoints : highScore = (score += linePoints).toString();

        // Считаем полученные деньги количество очков * стоимость шарика * коэффициент
        // Коэффициент получется делением количества шаров в линии на мин значние
        money += Math.floor(linePoints * (GameGlobals.instance.ballCosts * (ballsCoords.length / LINE_LENGTH)));

        // Check if user bet the king score
        bitTheRecord(score);

        trace("User score:", score, "User earn money", money, "Line points", linePoints, GameGlobals.instance.ballCosts, ballsCoords.length / LINE_LENGTH);

        while (ballsCoords.length > 0)
        {
            delayAnimation = counter;
            TweenMax.to(field[ballsCoords[0].x][ballsCoords[0].y].Ball, 0.4,
                    {
                        y: field[ballsCoords[0].x][ballsCoords[0].y].Ball.y - 5,
                        alpha: 0.0,
                        delay: delayAnimation * 0.15,
                        onComplete: hideBallsInLine,
                        onCompleteParams: [field[ballsCoords[0].x][ballsCoords[0].y].Ball, ballsCoords[0].x, ballsCoords[0].y, counter]
                    });

            ballsCoords.shift();
            counter++;
        }

        var percents:int = (score * 100) / Number(highScore);
        var pixels:int = (130 * percents) / 100;

        moneyTable.text = "Percents = " + percents.toString() + " _ pixels = " + pixels + " *** => " + money;

        scoreAdds.text = "+" + linePoints.toString();

        TweenMax.to(scoreAdds, 1, {y: -10, alpha: 0, onComplete: function ():void
        {
            scoreAdds.y = 8.5;
            scoreAdds.alpha = 1;
            scoreAdds.text = "";
        }});

        TweenMax.to(pretenderBird, 0.5, {y: 210 - pixels, ease: Back.easeOut});
    }

    /**
     * If we press RESET game
     * but earn some money - show message
     * else just begin new game
     * */
    public function resetGame():void
    {
        if (_freeCells > 0)
        {
            if (score > 0 && score < Number(highScore)) updateUserScore(score);
            checkKing("restart-game");

            beginGame();
        }
        else
        {
            beginGame();
        }
    }

    protected function bitTheRecord(userScore:Number):void
    {
        if (userScore > Number(highScore) && pretenderBird.visible)
        {
            hidePretenderGraphic();
            moveScoreAnimationTo(false, true);

            highScore = userScore.toString();

            updateUserScore(userScore);
            checkKing("bit-record");

            MessageManager.instance.addSystemMessage([ pretenderName + ", поздравляем! \nТеперь Вы наш новый король!"], false, true);
        }
    }

    protected function updateUserScore(value:Number):void
    {
        var userScore:IScore = new ScoreDO();
        userScore.id = GameGlobals.instance.vkUserId.toString();
        userScore.name = Server.idToVkontakteName(GameGlobals.instance.vkUserId);
        userScore.points = value;

        GameGlobals.instance.scores.push(userScore);
        GameGlobals.instance.sortScoresDescending();
        GameGlobals.instance.scores.pop();

        userScore = null;

        highScore = GameGlobals.instance.scores[0].points.toString();

        TweenMax.to(pretenderBird, 0.5, {y: 210, ease: Back.easeOut});

        var api_query:String = "https://api.vk.com/method/storage.set?uid=" + GameGlobals.instance.vkUserId +
                "&key=scope&value=" + JSON.stringify(GameGlobals.instance.jsonscores()) +
                "&global=1&access_token=" + GameGlobals.instance.vkSecret;
        var api_request:URLRequest = new URLRequest(api_query);
        var api_loader:URLLoader = new URLLoader();
        api_loader.load(api_request);
    }

    protected function initScoresScope():void
    {
        var api_query:String = "https://api.vk.com/method/storage.get?uid=" + GameGlobals.instance.vkUserId +
                "&key=scope&&global=1&access_token=" + GameGlobals.instance.vkSecret;
        var api_request:URLRequest = new URLRequest(api_query);
        var api_loader:URLLoader = new URLLoader();
        api_loader.addEventListener(Event.COMPLETE, function (event:Event):void
        {
            var result:Object = JSON.parse(event.currentTarget.data);
            if (result.response != "")
            {
                var records_l:Object = JSON.parse(result.response);
                GameGlobals.instance.scores = records_l.scope;
                highScore = GameGlobals.instance.scores[0].points.toString();

                checkKing();

                trace("initScoresScope", pretenderName, kingName);
            }
        });
        api_loader.load(api_request);
    }

    /**
     * checkKing()
     *
     *
     */
    private function checkKing(gameCase:String = "default"):void
    {
        switch (gameCase)
        {
            case "default":
                kingName = String(GameGlobals.instance.scores[0].name).split(" ")[0];
                pretenderName = String(Server.app_user_info.name).split(" ")[0];
                break;

            case "bit-record":
                kingName = pretenderName;
                break;

            case "restart-game":
                showPretenderGraphic();
                moveScoreAnimationTo(true);
                break;

        }
    }

    /**
     * hideBallsInLine()
     * @param obj
     * @param x
     * @param y
     * @param counter
     *
     */
    private function hideBallsInLine(obj:Ball, _x:int, _y:int, counter:int):void
    {
        if (obj != null && fieldUI.getElementIndex(field[_x][_y].Ball as IVisualElement) != -1)
        {

            var bird:MovieClip = (field[_x][_y].Ball as Ball).bird[birdMcNames[field[_x][_y].ballColor]];
            birdAnimation.killClip(bird);

            var tempCell:Cell = field[_x][_y].cellUI;

            field[_x][_y] = new Object();

            field[_x][_y].cellUI = tempCell;
            field[_x][_y].allow = 0;
            field[_x][_y].ballColor = 0;
            field[_x][_y].cellUI.stateIndicator = 0;

            obj.removeEventListener(MouseEvent.MOUSE_DOWN, selectBallItem);
            fieldUI.removeElement(obj);

            _score++;
            _freeCells++;
        }
    }


    /* ===================================================================================================== */

    private function hidePretenderGraphic():void
    {
        TweenMax.to(pretenderBird, 1, {alpha: 0, colorTransform: {tint: 0xffffff, tintAmount: 1}, onComplete: function ():void { pretenderBird.visible = false }});
        TweenMax.to(tf_pretenderName, 1.2, {alpha: 0, colorTransform: {tint: 0xffffff, tintAmount: 1}});
        TweenMax.to(scoreUserField, 1.4, {alpha: 0, colorTransform: {tint: 0xffffff, tintAmount: 1}});
    }

    private function showPretenderGraphic():void
    {
        TweenMax.to(pretenderBird, .1, {alpha: 1, colorTransform: {tint: 0xffffff, tintAmount: 0}, onComplete: function ():void { pretenderBird.visible = true }});
        TweenMax.to(tf_pretenderName, .1, {alpha: 1, colorTransform: {tint: 0xffffff, tintAmount: 0}});
        TweenMax.to(scoreUserField, .1, {alpha: 1, colorTransform: {tint: 0xffffff, tintAmount: 0}});
    }

    private function moveScoreAnimationTo(right:Boolean = false, left:Boolean = false):void
    {
        if (right)
        {
            scoreAdds.x = 494;
            scoreAdds.setStyle("textAlign", "right");
        }
        else
        {
            scoreAdds.x = 21;
            scoreAdds.setStyle("textAlign", "left");
        }
    }


    /**
     * Unselect()
     *
     */
    private function Unselect():void
    {
        _enabled = true;

        if (0 <= _currentX)
        {
            _currentX = _currentY = -999;
        }
        for (var i:int = 0; i < FIELD_WIDTH; i++)
        {
            for (var j:int = 0; j < FIELD_HEIGHT; j++)
            {
                if (field[i][j].allow != -9)
                {
                    field[i][j].allow = 0;
                    field[i][j].cellUI.stateIndicator = 0;
                }
                else
                {
                    (field[i][j].Ball as Ball).filters = [unselectCellGlow];

                }
            }
        }

    }

    // Generate random coords
    private function generateCoords():Point
    {
        var coords:Point = new Point();

        coords.x = Math.floor(Math.random() * FIELD_WIDTH);
        coords.y = Math.floor(Math.random() * FIELD_HEIGHT);

        return coords;
    }

    // Find correct path
    /**
     * getFinishCoord()
     * @param wave
     * @param forCheck
     *
     */
    private function getFinishCoord(wave:int, forCheck:ArrayCollection):void
    {

        if (forCheck == null || forCheck.length == 0) return;

        var nForCheck:ArrayCollection = new ArrayCollection();

        for each (var item:Object in forCheck)
        {
            var x:int = item.x;
            var y:int = item.y;

            if (y - 1 >= 0)
            {
                if (field[x][y - 1].allow == -1)
                {
                    start = {x: x, y: y - 1};
                    trace("coords = " + start.x, start.y);
                    _pathNotEmpty = false;
                    return;
                }

                if (field[x][y - 1].allow == 0)
                {
                    field[x][y - 1].allow = wave;
                    nForCheck.addItem({x: x, y: y - 1});
                }
            }

            if (x - 1 >= 0)
            {
                if (field[x - 1][y].allow == -1)
                {
                    start = {x: x - 1, y: y};
                    trace("coords = " + start.x, start.y);
                    _pathNotEmpty = false;
                    return;
                }

                if (field[x - 1][y].allow == 0)
                {
                    field[x - 1][y].allow = wave;
                    nForCheck.addItem({x: x - 1, y: y});
                }
            }

            if (y + 1 < FIELD_HEIGHT)
            {
                if (field[x][y + 1].allow == -1)
                {
                    start = {x: x, y: y + 1};
                    trace("coords = " + start.x, start.y);
                    _pathNotEmpty = false;
                    return;
                }

                if (field[x][y + 1].allow == 0)
                {
                    field[x][y + 1].allow = wave;
                    nForCheck.addItem({x: x, y: y + 1});
                }
            }

            if (x + 1 < FIELD_WIDTH)
            {
                if (field[x + 1][y].allow == -1)
                {
                    start = {x: x + 1, y: y};
                    trace("coords = " + start.x, start.y);
                    _pathNotEmpty = false;
                    return;
                }

                if (field[x + 1][y].allow == 0)
                {
                    field[x + 1][y].allow = wave;
                    nForCheck.addItem({x: x + 1, y: y});
                }
            }

        }

        // DELETE: Show test info in cells
        /*for each(var i:* in nForCheck)
         field[i.x][i.y].cellUI.stateIndicator = "_"+wave;*/

        getFinishCoord(wave + 1, nForCheck);
    }

    /**
     * makeOurPath()
     * @param startPoint
     * @return
     *
     */
    private function makeOurPath(startPoint:Object):ArrayCollection
    {
        var path:ArrayCollection = new ArrayCollection();

        if (startPoint == null) return null;

        var x:int = startPoint.x;
        var y:int = startPoint.y;
        var currentPos:int = 9999;
        var i:int = 0;

        while (i++ < 900)
        {

            if ((y + 1) < FIELD_HEIGHT && field[x][y + 1].allow > 0 && field[x][y + 1].allow < currentPos)
            {
                currentPos = field[x][y + 1].allow;
                field[x][y + 1].allow = -4;
                path.addItem({x: x, y: y + 1});
                y++;
            }
            else if ((x + 1) < FIELD_WIDTH && field[x + 1][y].allow > 0 && field[x + 1][y].allow < currentPos)
            {
                currentPos = field[x + 1][y].allow;
                field[x + 1][y].allow = -4;
                path.addItem({x: x + 1, y: y});
                x++;
            }
            else if ((y - 1) >= 0 && field[x][y - 1].allow > 0 && field[x][y - 1].allow < currentPos)
            {
                currentPos = field[x][y - 1].allow;
                field[x][y - 1].allow = -4;
                path.addItem({x: x, y: y - 1});
                y--;
            }
            else if ((x - 1) >= 0 && field[x - 1][y].allow > 0 && field[x - 1][y].allow < currentPos)
            {
                currentPos = field[x - 1][y].allow;
                field[x - 1][y].allow = -4;
                path.addItem({x: x - 1, y: y});
                x--;
            }
            else if (field[x][y].allow == -1)
            {
                currentPos = field[x][y].allow;
                field[x][y].allow = -4;
                path.addItem({x: x, y: y});

            }

            if (currentPos == 1 || currentPos == 9999)
            {
                break;
            }
        }

        trace("GET PATH => " + x, y)

        if (path.length == 0) path.addItem({label: "NO_PATH"});

        return path;
    }


    /**
     * searchLines()
     * @param arrivedX
     * @param arrivedY
     * @return
     *
     */
    private function searchLines(arrivedX:int, arrivedY:int):Array
    {

        trace("****> SEARCH LINE INITIATED <****", arrivedX, arrivedY)

        // Храним цвет выбранного шарика для поиска линий
        var isSameColor:int = field[arrivedX][arrivedY].ballColor;

        // Направление поиска линии [1, 0], [1, 1], [0, 1], [1, -1]
        var way:Array = [
            [1, 0],
            [1, 1],
            [0, 1],
            [-1, 1]
        ];
        //Начальные координаты
        var localX:int = arrivedX;
        var localY:int = arrivedY;
        var lines:Array = new Array();

        for (var count:int = 4; count > 0; count--)
        {

            var p:Array = way[count - 1]; // Выберем текущее направление

            var ballInLine:int = 1;  // Количество шаров одного цвета в линии, пока он один
            var x:int = localX
            var y:int = localY;  // Координаты текущего шара
            var dx:int = p[0];
            var dy:int = p[1]; // Направление движения, мы будем каждый раз изменять координаты на эти значения

            var line:Array = [
                {_x: localX, _y: localY}
            ];

            do {
                x = x + dx;
                y = y + dy;

                if (x < FIELD_WIDTH && y < FIELD_HEIGHT && y >= 0 && x >= 0)
                {
                    if (field[arrivedX][arrivedY].ballColor != field[x][y].ballColor || field[x][y].allow != -9) break;
                }
                else
                {
                    break;
                }

                line.push({_x: x, _y: y});
                ballInLine++;

            } while (ballInLine < LINE_LENGTH);


            // Половина линии мы подсчитали теперь вторая
            x = localX;
            y = localY;  // Вернемся в начало
            dx = -p[0];
            dy = -p[1];  // инвертируем направление
            ballInLine = 1;  // Сбросим счетчик

            do {
                x = x + dx;
                y = y + dy;

                if (x < FIELD_WIDTH && y < FIELD_HEIGHT && x >= 0 && y >= 0)
                {
                    if (field[arrivedX][arrivedY].ballColor != field[x][y].ballColor || field[x][y].allow != -9) break;
                }
                else
                {
                    break;
                }

                line.push({_x: x, _y: y});
                ballInLine++;

            } while (ballInLine < LINE_LENGTH);

            if (line.length >= LINE_LENGTH) lines.push(line);  // А теперь если длина линии больше или равна минимальной добавим ее в массив линий
        }

        return lines;
    }

}
}



