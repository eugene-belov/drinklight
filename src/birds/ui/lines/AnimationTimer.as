package birds.ui.lines
{
import flash.display.MovieClip;
import flash.events.TimerEvent;
import flash.utils.Timer;
import flash.utils.setTimeout;

public class AnimationTimer extends Lines
{
    protected var stackOfAnimators:Array = new Array();
    protected var kingTime:Timer;
    protected var pretenderTime:Timer;

    protected var MIN:Number = 6000;

    protected var _object:MovieClip;
    protected var _objectsToAnimate:Array = new Array();

    /* Constants of labels in a amination lib */
    protected const BIRD_HMUR:String = "hmur";
    protected const BIRD_JUMP:String = "jump";
    protected const BIRD_MORGAET:String = "morgaet";
    protected const BIRD_NEGATIVE:String = "negative";
    protected const BIRD_SLEEP:String = "sleep";

    public static const PRETENDER_BEER:String = "beer";
    public static const PRETENDER_STEP:String = "step";
    public static const PRETENDER_VZGLYAD:String = "vzglyad";

    public static const KING_MORGAET:String = "morgaet";
    public static const KING_POSOH:String = "posoh";
    public static const KING_VZDIHAET:String = "vzdihaet";
    public static const KING_KORONA:String = "korona";

    public function AnimationTimer(object:MovieClip = null)
    {
        super();
        _object = object;
        _objectsToAnimate = new Array();
    }

    /**
     * startKingAnimation()
     *
     */
    public function startKingAnimation():void
    {
        var morgaet:Timer = new Timer(MIN * 2);
        morgaet.addEventListener(TimerEvent.TIMER, function ():void
        {
            morgaet.delay = ( Math.floor(Math.random() * (MIN * 2) + (MIN / 2)) );

            _object.gotoAndPlay(KING_MORGAET);
        }, false, 0, true);
        morgaet.start();

        var posoh:Timer = new Timer(MIN * 15);
        posoh.addEventListener(TimerEvent.TIMER, function ():void
        {
            posoh.delay = ( Math.floor(Math.random() * (MIN * 15) + (MIN * 7)) );

            _object.gotoAndPlay(KING_POSOH);
        }, false, 0, true);
        posoh.start();

        var korona:Timer = new Timer(MIN * 7);
        korona.addEventListener(TimerEvent.TIMER, function ():void
        {
            korona.delay = ( Math.floor(Math.random() * (MIN * 7) + (MIN * 3)) );

            _object.gotoAndPlay(KING_KORONA);
        }, false, 0, true);
        korona.start();

        var vzdoh:Timer = new Timer(MIN * 4);
        vzdoh.addEventListener(TimerEvent.TIMER, function ():void
        {
            vzdoh.delay = ( Math.floor(Math.random() * (MIN * 4) + (MIN * 2)) );

            _object.gotoAndPlay(KING_VZDIHAET);
        }, false, 0, true);
        vzdoh.start();
    }

    /**
     * startPretenderAnimation()
     *
     */
    public function startPretenderAnimation():void
    {
        var beer:Timer = new Timer(MIN * 4);
        beer.addEventListener(TimerEvent.TIMER, function ():void
        {
            beer.delay = ( Math.floor(Math.random() * (MIN * 4) + (MIN * 2)) );

            _object.gotoAndPlay(PRETENDER_BEER);
        }, false, 0, true);
        beer.start();

        var step:Timer = new Timer(MIN * 7);
        step.addEventListener(TimerEvent.TIMER, function ():void
        {
            step.delay = ( Math.floor(Math.random() * (MIN * 7) + (MIN * 5)) );

            _object.gotoAndPlay(PRETENDER_STEP);
        }, false, 0, true);
        step.start();

        var look:Timer = new Timer(MIN * 10);
        look.addEventListener(TimerEvent.TIMER, function ():void
        {
            look.delay = ( Math.floor(Math.random() * (MIN * 10) + (MIN * 7)) );

            _object.gotoAndPlay(PRETENDER_VZGLYAD);
        }, false, 0, true);
        look.start();

    }

    /**
     * animationInGame()
     *
     */
    public function startGameBirdsAnimation():void
    {
        trace("In game animation");

        var birdsLook:Timer = new Timer(MIN * 2);
        birdsLook.addEventListener(TimerEvent.TIMER, function ():void
        {
            birdsLook.delay = ( Math.floor(Math.random() * (MIN * 2) + (MIN / 2)) );

            var index:int = Math.random() * _objectsToAnimate.length;
            if (_objectsToAnimate.length > 0 && _objectsToAnimate[index].isSleep == false && _objectsToAnimate[index].isMoving == false)
            {
                (_objectsToAnimate[index].data as MovieClip).gotoAndPlay(BIRD_MORGAET);
            }

        }, false, 0, true);
        birdsLook.start();

        var birdsHmur:Timer = new Timer(MIN * 4);
        birdsHmur.addEventListener(TimerEvent.TIMER, function ():void
        {
            birdsHmur.delay = ( Math.floor(Math.random() * (MIN * 4) + (MIN / 2)) );

            var index:int = Math.random() * _objectsToAnimate.length;
            if (_objectsToAnimate.length > 0 && _objectsToAnimate[index].isSleep == false && _objectsToAnimate[index].isMoving == false)
            {
                (_objectsToAnimate[index].data as MovieClip).gotoAndPlay(BIRD_HMUR);
            }

        }, false, 0, true);
        birdsHmur.start();

    }

    /**
     * wakeUpAndSleep
     * @param obj as MovieClip
     *
     */
    public function wakeUpAndSleep(obj:MovieClip):void
    {
        for (var itm:* in _objectsToAnimate)
        {
            if (_objectsToAnimate[itm].data == obj)
            {
                _objectsToAnimate[itm].isSleep = false;
                sleep(itm);
                break;
            }
        }
    }

    /**
     * unselectBird
     * @param obj
     *
     */
    public function unselectBird(obj:MovieClip):void
    {
        for (var itm:* in _objectsToAnimate)
        {
            if (_objectsToAnimate[itm].data == obj && _objectsToAnimate[itm].isSleep == false)
            {
                (_objectsToAnimate[itm].data as MovieClip).gotoAndStop(1);
            }
        }
    }

    /**
     * sleep
     * @param index
     *
     */
    protected function sleep(index:int):void
    {
        var birdTime:Timer = new Timer(MIN * 10);
        var functionLocal:Function;

        birdTime.addEventListener(TimerEvent.TIMER, functionLocal = function ():void
        {
            if (_objectsToAnimate[index].isMoving == false)
            {
                _objectsToAnimate[index].isSleep = true;
                (_objectsToAnimate[index].data as MovieClip).gotoAndPlay(BIRD_SLEEP);

                setTimeout(function ():void
                {
                    (_objectsToAnimate[index].data as MovieClip).gotoAndStop(115);
                }, 3000);
            }

            birdTime.removeEventListener(TimerEvent.TIMER, functionLocal);
            birdTime.stop();
        }, false, 0, true);
        birdTime.start();
    }

    /**
     * addClip
     * @param obj as MovieClip
     *
     */
    public function addClip(obj:MovieClip):void
    {
        _objectsToAnimate.push({data: obj, isSleep: false, isMoving: false});

        sleep(_objectsToAnimate.length - 1);
    }

    public function beginBirdRunning(obj:MovieClip):void
    {
        for (var itm:* in _objectsToAnimate)
        {
            if (_objectsToAnimate[itm].data == obj)
            {
                _objectsToAnimate[itm].isMoving = true;
                break;
            }
        }
    }

    public function stopBirdRunning(obj:MovieClip):void
    {
        for (var itm:* in _objectsToAnimate)
        {
            if (_objectsToAnimate[itm].data == obj)
            {
                _objectsToAnimate[itm].isMoving = false;
                break;
            }
        }
    }

    /**
     * killClip
     * @param obj as MovieClip
     *
     */
    public function killClip(obj:MovieClip):void
    {
        for (var itm:* in _objectsToAnimate)
        {
            if (_objectsToAnimate[itm] == obj)
            {
                _objectsToAnimate.splice(itm, 1);
                break;
            }
        }

    }
}
}